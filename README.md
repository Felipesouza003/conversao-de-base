# Algoritmo para Conversão de Base - Método Divisão Sucessivas

## Descrição


## Como utilizar o CONVERSOR DE BASE

Para usar a biblioteca adicione o `convert.h` no seu código:

```c
#include <stdio.h>
#include "convert.h"
```


Para rodar exemplo, use o seguinte comando:

```bash
gcc ./src/main.c -I ./Include/ -o main
./main
```


## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/2578394/avatar.png?width=400)  | Jeferson Lima | [@jeferson.lima](https://gitlab.com/jeferson.lima) | [jeferson.lima@utfpr.edu.br](mailto:jeferson.lima@utfpr.edu.br)





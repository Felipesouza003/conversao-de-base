#ifndef CONVERT_H
#define CONVERT_H


#include <stdio.h>
#include <math.h>

int bin2dec(int bin[], int length)
{
  int dec = 0, i;

  for (i=0; i<length; i++)
  {
    dec = dec + (bin[i]*pow(2,length-i-1));
  }

  return dec;
}

#endif
